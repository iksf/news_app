package com.example.newsapplication;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MainActivityRecyclerAdapter extends RecyclerView.Adapter<MainActivityRecyclerAdapter.ViewHolder> {

    private final MainActivity mainActivity;
    private List<Article> articles;


    public MainActivityRecyclerAdapter(MainActivity m, List<Article> articles) {
        this.articles = articles;
        this.mainActivity = m;
    }

    void setArticles(List<Article> articles) {
        this.articles = articles;


        this.notifyDataSetChanged();
        Log.d("mlogger", "articles set with list of " + articles.size());
    }

    @NonNull
    @Override
    public MainActivityRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View contextView = inflater.inflate(R.layout.article_recycler_entry, parent, false);

        return new ViewHolder(contextView);
    }

    @Override
    public void onBindViewHolder(@NonNull MainActivityRecyclerAdapter.ViewHolder holder, int position) {


        Article article = this.articles.get(position);

        if (holder.text != null) {


            holder.text.setText(article.title);
            holder.text.setOnClickListener(v -> MainActivity.sendToArticlePage(mainActivity, article));
            holder.deleteQuery.setOnClickListener(v -> {
                Log.d("mlogger", "deleting query");
                new DeleteArticle(mainActivity).execute(article);
            });
        }

    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    static class DeleteArticle extends DatabaseTask<Article, Void, MainActivity> {

        DeleteArticle(MainActivity caller) {
            super(caller);
        }

        @Override
        protected Void doInBackground(Article... articles) {
            MainActivity act = caller.get();
            if (act != null) {
                for (Article a : articles) {
                    AppDatabase.getAppDatabase(act).articleDao().delete(a);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            MainActivity a = caller.get();
            if (a != null) {
                a.showArticles();
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView text;
        final Button deleteQuery;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.recycler_article_text);
            deleteQuery = itemView.findViewById(R.id.delete_entry_button);
        }
    }
}
