package com.example.newsapplication;

import java.util.List;

class ApiReply {
    String status;
    int totalresults;
    List<Article> articles;
}
