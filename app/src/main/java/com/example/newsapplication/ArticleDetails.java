package com.example.newsapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.ImageRequest;

import java.lang.ref.WeakReference;

public class ArticleDetails extends AppCompatActivity {

    private TextView titleTextView;
    private TextView descTextView;
    private TextView authorTextView;
    private ImageView imageView;
    private Button goToUrlButton;
    private String articleUrl = "";

    private void loadArticle(Article a) {

        WeakReference<ArticleDetails> act = new WeakReference<>(this);
        ImageRequest imageRequest = new ImageRequest(a.urlToImage, bitmap -> {

            ArticleDetails articleDetails = act.get();
            if (articleDetails == null) {
                return;
            }

            articleDetails.imageView.setImageBitmap(bitmap);

        }, 0, 0, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.ARGB_8888, error -> {
            ArticleDetails articleDetails = act.get();
            if (articleDetails != null) {
                Toast.makeText(articleDetails, "Unable to get image for this article", Toast.LENGTH_SHORT).show();
            }
        });

        VolleyQueue.getInstance(this).addToRequestQueue(imageRequest);


        Log.d("mlogger", a.title + "\n" + a.author + "\n" + a.description + "\n" + a.urlToImage);

        this.titleTextView = findViewById(R.id.title_text);
        this.authorTextView = findViewById(R.id.author_text);
        this.descTextView = findViewById(R.id.recycler_article_text);
        this.imageView = findViewById(R.id.article_image);
        this.titleTextView.setText(a.title);
        this.descTextView.setText(a.description);
        this.authorTextView.setText(a.author);
        this.articleUrl = a.url;


    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_details);
        this.titleTextView = findViewById(R.id.title_text);
        this.authorTextView = findViewById(R.id.author_text);
        this.descTextView = findViewById(R.id.recycler_article_text);
        this.imageView = findViewById(R.id.article_image);
        this.goToUrlButton = findViewById(R.id.url_button);

        WeakReference<ArticleDetails> r = new WeakReference<>(this);
        this.goToUrlButton.setOnClickListener(view -> {
            ArticleDetails a = r.get();
            if (a != null) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(a.articleUrl));
                startActivity(browserIntent);

            }
        });
        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        if (title == null) {
            finish();
            return;
        }

        new GetArticleByTitle(this).execute(title);


    }

    static class GetArticleByTitle extends DatabaseTask<String, Article, ArticleDetails> {

        GetArticleByTitle(ArticleDetails caller) {
            super(caller);

        }

        @Override
        protected Article doInBackground(String... strings) {
            ArticleDetails a = caller.get();
            if (a != null) {
                Article b = AppDatabase.getAppDatabase(a).articleDao().getByTitle(strings[0]);
                if (b != null) {
                    return b;
                }
                a.finish();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Article article) {
            ArticleDetails a = caller.get();
            assert article != null;

            super.onPostExecute(article);
            if (a == null) {
                Log.d("mlogger", "app closed before asynctask complete");
                return;

            }

            a.loadArticle(article);


        }
    }
}
