package com.example.newsapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SavedQueriesActivity extends AppCompatActivity {


    RecyclerView m_recycler_view;


    private Button clearDbButton;
    private Button addButton;
    private TextView newQueryEntry;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_queries);


        m_recycler_view = findViewById(R.id.queries_recycler);
        SavedQueriesRecyclerAdapter adapter = new SavedQueriesRecyclerAdapter(this);
        m_recycler_view.setAdapter(adapter);

        m_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        clearDbButton = findViewById(R.id.clear_db_button);
        clearDbButton.setOnClickListener(view -> new ClearDb(this).execute());
        addButton = findViewById(R.id.recycler_final_button);

        newQueryEntry = findViewById(R.id.recycler_final_edit_text);
    }

    private void addEntry() {
        String s = newQueryEntry.getText().toString();
        SharedPreferences sp = getSharedPreferences("app", Context.MODE_PRIVATE);


        Set<String> prefs = sp.getStringSet("queries", new HashSet<>());
        prefs.add(s);
        SharedPreferences.Editor editor = sp.edit();


        editor.clear();
        editor.putStringSet("queries", prefs);
        editor.apply();
        update();
        newQueryEntry.setText("");

    }

    @Override
    protected void onStart() {
        super.onStart();

        addButton.setOnClickListener(l -> addEntry());
    }

    private void update() {
        Set<String> ss = getSharedPreferences("app", Context.MODE_PRIVATE).getStringSet("queries", new HashSet<>());
        ((SavedQueriesRecyclerAdapter) Objects.requireNonNull(this.m_recycler_view.getAdapter())).setQueries(new ArrayList<>(ss));
    }

    static class ClearDb extends DatabaseTask<Void, Void, SavedQueriesActivity> {

        ClearDb(SavedQueriesActivity caller) {
            super(caller);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            AppDatabase.getAppDatabase(caller.get()).articleDao().dropAll();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            SavedQueriesActivity a = caller.get();
            if (a != null) {
                Objects.requireNonNull(a.m_recycler_view.getAdapter()).notifyDataSetChanged();
            }
        }
    }


}
