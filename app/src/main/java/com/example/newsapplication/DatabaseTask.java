package com.example.newsapplication;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

abstract class DatabaseTask<T, U, V> extends AsyncTask<T, Void, U> {
    final WeakReference<V> caller;

    DatabaseTask(V caller) {
        this.caller = new WeakReference<>(caller);
    }


}
