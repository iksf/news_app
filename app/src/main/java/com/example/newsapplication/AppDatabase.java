package com.example.newsapplication;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Article.class}, version = 14)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase ourInstance;

    static synchronized AppDatabase getAppDatabase(@NonNull Context context) {
        if (ourInstance == null)
            ourInstance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "NEWSAPP_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build();
        return ourInstance;
    }

    abstract ArticleDao articleDao();


}



