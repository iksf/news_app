package com.example.newsapplication;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.json.JSONException;
import org.json.JSONObject;

@Entity
public class Article {


    @PrimaryKey
    @NonNull
    String title;

    String author, description, url, urlToImage;

    Article(JSONObject o) {
        try {
            author = o.getString("author");
            title = o.getString("title");
            description = o.getString("description");
            url = o.getString("url");
            urlToImage = o.getString("urlToImage");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    Article(String author, String title, String description, String url, String urlToImage) {
        this.author = author;
        this.title = title;
        this.description = description;
        this.url = url;
        this.urlToImage = urlToImage;
    }

}
