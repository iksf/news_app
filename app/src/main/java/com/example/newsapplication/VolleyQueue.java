package com.example.newsapplication;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyQueue {
    private static VolleyQueue instance;
    private static Context context;
    private RequestQueue volleyRequestQueue;

    private VolleyQueue(Context ctx) {
        context = ctx;
        volleyRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyQueue getInstance(Context ctx) {
        if (instance == null) {
            context = ctx;
            instance = new VolleyQueue(context);
        }
        return instance;
    }

    private RequestQueue getRequestQueue() {
        if (volleyRequestQueue == null)
            volleyRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        return volleyRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }

}
