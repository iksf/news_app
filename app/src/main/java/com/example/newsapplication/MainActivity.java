package com.example.newsapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class MainActivity extends AppCompatActivity {


    private RecyclerView m_recycler;

    static void sendToArticlePage(Context c, Article article) {
        Intent intent = new Intent(c, ArticleDetails.class);
        intent.putExtra("title", article.title);
        Log.d("mlogger", "starting article " + article.title);
        c.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_recycler = findViewById(R.id.main_recycler);

        m_recycler.setAdapter(new MainActivityRecyclerAdapter(this, new ArrayList<>()));
        m_recycler.setLayoutManager(new LinearLayoutManager(this));
        findViewById(R.id.preferences_button).setOnClickListener(v -> startActivity(
                new Intent(v.getContext(), SavedQueriesActivity.class)));
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_recycler = findViewById(R.id.main_recycler);
        this.initialiseDownload();
        this.showArticles();

    }

    private String getApiKey() {
        return getSharedPreferences("app", Context.MODE_PRIVATE).getString("apikey", "b0622a8909e5485784ea083e9fb7c243");
    }

    private void performRequest(@NonNull String url, @NonNull String apikey, @NonNull String q) {
        String u = url + "?apikey=" + apikey + "&q=" + q;
        JsonObjectRequest r = new JsonObjectRequest(Request.Method.GET, u, null, ok -> {

            Gson gson = new Gson();
            ApiReply articles = gson.fromJson(ok.toString(), ApiReply.class);
            new UpdateDatabase(this).execute(articles);
        }, err -> {
            Toast toast = Toast.makeText(this, "Unable to connect to newsapi", Toast.LENGTH_LONG);
            toast.show();
        });


        VolleyQueue.getInstance(this).addToRequestQueue(r);

    }

    private void initialiseDownload() {

        String url = getSharedPreferences("app", Context.MODE_PRIVATE).getString("url", "https://newsapi.org/v2/everything");
        String apikey = getApiKey();
        Set<String> queries = getSharedPreferences("app", Context.MODE_PRIVATE).getStringSet(
                "queries", new HashSet<>()
        );

        queries.forEach(q -> {
            Log.d("mlogger", "query:" + q);
            this.performRequest(url, apikey, q);
        });


    }

    void showArticles() {
        new GetArticles(this).execute();
    }

    static class UpdateDatabase extends DatabaseTask<ApiReply, Void, MainActivity> {
        UpdateDatabase(MainActivity caller) {
            super(caller);
        }

        @Override
        protected Void doInBackground(ApiReply... a) {
            for (ApiReply b : a) {

                for (Article article : b.articles) {
                    AppDatabase.getAppDatabase(caller.get()).articleDao().insert(article);
                    Log.d("mlogger", "inserted article");
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            caller.get().showArticles();
            Log.d("mlogger", "show new articles");

        }
    }

    class GetArticles extends DatabaseTask<Void, List<Article>, MainActivity> {
        GetArticles(MainActivity caller) {
            super(caller);
        }

        @Override
        protected List<Article> doInBackground(Void... voids) {

            MainActivity m = this.caller.get();
            if (m != null) {

                return AppDatabase.getAppDatabase(m).articleDao().getAll();
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<Article> articles) {
            super.onPostExecute(articles);

            Log.d("mlogger", "post execute populatelist");
            MainActivity m = this.caller.get();
            if (m == null || articles == null) {
                return;
            }
            ((MainActivityRecyclerAdapter) Objects.requireNonNull(m.m_recycler.getAdapter())).setArticles(articles);
        }
    }

}

