package com.example.newsapplication;

import android.content.Context;
import android.util.ArraySet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class SavedQueriesRecyclerAdapter extends RecyclerView.Adapter<SavedQueriesRecyclerAdapter.ViewHolder> {
    private final SavedQueriesActivity m_activity;
    private List<String> m_saved_queries;


    public SavedQueriesRecyclerAdapter(SavedQueriesActivity activity) {
        this.m_activity = activity;


        this.m_saved_queries = new ArrayList<>(m_activity.getSharedPreferences("app", Context.MODE_PRIVATE).getStringSet("queries", new ArraySet<>()));

    }

    public void setQueries(List<String> q) {
        this.m_saved_queries = q;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contextView = inflater.inflate(R.layout.query_recycler_entry, parent, false);
        return new ViewHolder(contextView);
    }

    @Override
    public void onBindViewHolder(@NonNull SavedQueriesRecyclerAdapter.ViewHolder viewHolder, int position) {
        String entry = this.m_saved_queries.get(position);
        Button deleteButton = viewHolder.deleteButton;
        TextView textView = viewHolder.searchQueryView;
        textView.setText(entry);
        deleteButton.setOnClickListener((View v) -> {
            if (!(position > m_saved_queries.size() - 1)) {
                this.m_saved_queries.remove(position);
                this.m_activity.getSharedPreferences("app", Context.MODE_PRIVATE).edit().putStringSet("queries", new HashSet<>(this.m_saved_queries)).apply();
                Objects.requireNonNull(this.m_activity.m_recycler_view.getAdapter()).notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return this.m_saved_queries.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView searchQueryView;
        final Button deleteButton;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.searchQueryView = itemView.findViewById(R.id.entry_query_name);
            this.deleteButton = itemView.findViewById(R.id.query_delete);
        }
    }
}
