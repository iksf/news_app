package com.example.newsapplication;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ArticleDao {

    @Query("SELECT * FROM Article")
    List<Article> getAll();


    @Query("DELETE FROM Article")
    void dropAll();

    @Query("SELECT * FROM Article WHERE Article.title LIKE :title LIMIT 1")
    Article getByTitle(String title);


    @Delete
    void delete(Article a);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Article a);

}
